# returns if a number is a palindrome
paliList = []
rangelist = []
lastPaliIndex = 0
value = 0

def isPalindrome(num):
	numStr = str(num)
	revNumStr = ''
	for i in reversed(numStr):
		revNumStr += str(i)
	if numStr == revNumStr:
		return True
	else:
		return False
# finds all the palindromes in a given range
def findPalindromesInRange(number):
	count = 1
	while count < value:
		if isPalindrome(count):
 			paliList.append(count)
		count += 1

def findNextPalindrome(currentNum):
	global lastPaliIndex
	if currentNum in paliList :
		lastPaliIndex = paliList.index(currentNum)
		return currentNum
	else :
		return paliList[lastPaliIndex + 1]
def distanceBetweenPalindromes(a, b):
	furthest = b - a - 1 # furthest from next pali
	furthest /= 2
	furthest *= (b - a)
	return furthest

def doWork():
	total = 0
	count = 1
	while count <= paliList[len(paliList)-1]:
		total += findNextPalindrome(count) - count
		count += 1
	return total

if __name__ == "__main__":
	value = 10 ** input("Enter max number> ")
	#rangeList = range(1,value)

	findPalindromesInRange(value)
	distanceBetweenPalindromes(11, 22)
	print doWork()
